/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var acceptableFileTypes = ['jpg', 'png'];  //List of acceptable files for a game board.
var activePiece = '';  //Storage for a game piece that has been clicked down on.

function main() {
    
    $('input[type=file][name=imgUpload]').on('change', function() {
        if( validateFile($(this).val(), acceptableFileTypes) ){
            $('#game_area').hide();
            $('.input').hide();
            initGameBoard(this.files[0]);
            $('#game_area').show();       
        }
    });
 
    $('#createBtn').on('click', function (){
        var name = $('#inputName');
        var persist = document.getElementById("namePersist").checked;
        
        if(validateName(name.val())){
            initPlayerPiece(name.val(), '#FFA500');
            if(!persist){
                name.val("");
            }
        }

    });
        
    $('#game_board_svg').on('mouseup', function (event) {
        activePiece = '';
    });

    $('#game_board_svg').on('mousemove', function (event) {
        var piece = activePiece;
        if(piece !== ''){         
            activePiece.attr({ cx: event.offsetX, cy: event.offsetY });
        }
    });
    
    $('#game_board_svg').on('mousedown', '.gamePiece', function (event) {
        activePiece = $(this);
    });
}

function validateName(name){
    
    if(name.length <= 0){
        alert("Please enter a name for the piece.");
        return false;
    }

    
    
    return true;
}

function validateFile(fileName, acceptableFileTypes){
    
    //Simple test to see if any file was selected at all.
    if(fileName === ''){
        alert('No file selected');
        return false;
    }
    
    //This block of code extracts the file name's extension before checking if it is in the list
    //of acceptable file extensions.
    var extension = [];
    var i = fileName.length-1;
    while(fileName[i] !== '.'){
        extension.unshift(fileName[i]);
        --i;
    }
    extension = extension.join("");
    if($.inArray(extension, acceptableFileTypes) === -1){
        alert('Unacceptable file type');
        return false;
    }
    
    return true;
}

function initPlayerPiece(name, color){
    
    //This will probably need to call a constructor to an object
    //that can conatin all the svg elements that will make up the 
    //acutal game piece.
    
    var x = parseFloat($('#game_board').css('width')) /2;
    var y = parseFloat($('#game_board').css('height')) /2;
    
    var board = document.getElementById('game_board_svg');
    board.innerHTML = board.innerHTML + '<circle class="gamePiece" cx="'+x+'" cy="'+y+'" r="20" stroke="black" stroke-width="1" fill="'+color+'"/>';    
}
        
function initGameBoard(image){

    var reader = new FileReader();
    var img = new Image();  
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    var windowScale = 95/100;

    reader.onloadend = function () {
        document.getElementById('game_board').style.backgroundImage = 'url("' + reader.result + '")';
        img.src = $('#game_board').css('background-image').replace(/url\(|\)$|"/ig, '');
    };
    img.onload = function () {
        //This section needs to call an image previewer if that is where this ends up going. 
        var scale =  Math.min( Math.floor(((windowWidth * windowScale) / this.width) * 100), 
                        Math.floor(((windowHeight * windowScale) / this.height) * 100));
        if(scale >= 100){
            $('#game_board').css('width', this.width);
            $('#game_board').css('height', this.height);
            $('#game_board_svg').css('width', this.width);
            $('#game_board_svg').css('height', this.height);
        }
        else{
            $('#game_board').css('width', this.width * (scale/100));
            $('#game_board').css('height', this.height * (scale/100));
            $('#game_board_svg').css('width', this.width * (scale/100));
            $('#game_board_svg').css('height', this.height * (scale/100));
        } 
    };

    if (image) {
        reader.readAsDataURL(image);
    }
    
    $('#game_area').show();
}


$(document).ready(main);